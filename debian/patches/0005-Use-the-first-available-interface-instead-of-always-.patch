From: =?utf-8?b?0L3QsNCx?= <nabijaczleweli@nabijaczleweli.xyz>
Date: Tue, 24 Sep 2024 17:36:39 +0200
Subject: Use the first available interface instead of always eth0 (Closes:
 #816411)

---
 ether-wake.c | 21 ++++++++++++++++++---
 etherwake.8  |  6 +++---
 2 files changed, 21 insertions(+), 6 deletions(-)

diff --git a/ether-wake.c b/ether-wake.c
index 00ea53c..5839346 100644
--- a/ether-wake.c
+++ b/ether-wake.c
@@ -22,7 +22,7 @@ static char usage_msg[] =
 "	Options:\n"
 "		-b	Send wake-up packet to the broadcast address.\n"
 "		-D	Increase the debug level.\n"
-"		-i ifname	Use interface IFNAME instead of the default 'eth0'.\n"
+"		-i ifname	Use interface IFNAME instead of the default '%s'.\n"
 "		-p <pw>		Append the four or six byte password PW to the packet.\n"
 "					A password is only required for a few adapter types.\n"
 "					The password may be specified in ethernet hex format\n"
@@ -66,6 +66,7 @@ static char usage_msg[] =
 #include <errno.h>
 #include <ctype.h>
 #include <string.h>
+#include <ifaddrs.h>
 
 #if 0							/* Only exists on some versions. */
 #include <ioctls.h>
@@ -110,9 +111,20 @@ static int get_dest_addr(const char *arg, struct ether_addr *eaddr);
 static int get_fill(unsigned char *pkt, struct ether_addr *eaddr);
 static int get_wol_pw(const char *optarg);
 
+static const char *default_ifname(void) {
+	struct ifaddrs *itr = NULL;
+	if(getifaddrs(&itr) == -1)
+		return "eth0";
+
+	for(; itr; itr = itr->ifa_next)
+		if((itr->ifa_flags & IFF_UP) && (itr->ifa_flags & IFF_BROADCAST))
+			return itr->ifa_name;
+	return "eth0";
+}
+
 int main(int argc, char *argv[])
 {
-	char *ifname = "eth0";
+	const char *ifname = NULL;
 	int one = 1;				/* True, for socket options. */
 	int s;						/* Raw socket */
 	int errflag = 0, verbose = 0, do_version = 0;
@@ -131,7 +143,7 @@ int main(int argc, char *argv[])
 		case 'D': debug++;			break;
 		case 'i': ifname = optarg;	break;
 		case 'p': get_wol_pw(optarg); break;
-		case 'u': printf("%s",usage_msg); return 0;
+		case 'u': printf(usage_msg, default_ifname()); return 0;
 		case 'v': verbose++;		break;
 		case 'V': do_version++;		break;
 		case '?':
@@ -149,6 +161,9 @@ int main(int argc, char *argv[])
 		return 3;
 	}
 
+	if (!ifname)
+		ifname = default_ifname();
+
 	/* Note: PF_INET, SOCK_DGRAM, IPPROTO_UDP would allow SIOCGIFHWADDR to
 	   work as non-root, but we need SOCK_PACKET to specify the Ethernet
 	   destination address. */
diff --git a/etherwake.8 b/etherwake.8
index 5f807c5..53190b9 100644
--- a/etherwake.8
+++ b/etherwake.8
@@ -26,9 +26,9 @@ .SH DESCRIPTION
 command.
 .PP
 .\" TeX users may be more comfortable with the \fB<whatever>\fP and
-.\" \fI<whatever>\fP escape sequences to invode bold face and italics, 
+.\" \fI<whatever>\fP escape sequences to invode bold face and italics,
 .\" respectively.
-\fBetherwake\fP is a program that generates and transmits a Wake-On-LAN 
+\fBetherwake\fP is a program that generates and transmits a Wake-On-LAN
 (WOL) "Magic Packet", used for restarting machines that have been
 soft-powered-down (ACPI D3-warm state). It generates the standard
 AMD Magic Packet format, optionally with a password included.  The
@@ -49,7 +49,7 @@ .SH OPTIONS
 Increase the Debug Level.
 .TP
 .B \-i ifname
-Use interface ifname instead of the default "eth0".
+Use interface ifname instead of the default (first available interface or "eth0").
 .TP
 .B \-p passwd
 Append a four or six byte password to the packet. Only a few adapters
